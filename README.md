# @CentryBot 

Telegram bot which checks if my roommates are currently at 
home: retrieves a list of current DHCP leases from the router and checks which 
hosts are online.

![Screenshot](http://i68.tinypic.com/307z11z.jpg)

### Implemented with:

* [Requests](http://docs.python-requests.org/en/master/)
* [Paramiko](http://www.paramiko.org/)

