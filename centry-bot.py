import json
import re
import subprocess
import time

import requests
from paramiko import AutoAddPolicy
from paramiko import SSHClient

TOKEN = "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)


def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js


def get_updates():
    url = URL + "getUpdates"
    js = get_json_from_url(url)
    return js


def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)


def send_message(text, chat_id):
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)


text, chat = get_last_chat_id_and_text(get_updates())

if chat == 82788258:
    if text == "/scan":

        ssh = SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(AutoAddPolicy())

        ssh.connect("192.168.2.1", username="username", password="password")

        shell = ssh.invoke_shell()
        time.sleep(0.25)
        shell.send("show dhcp leases | awk '{print $1,$6}\'\n")  # retrieving active DHCP leases from EdgeRouter™ X
        time.sleep(0.25)

        output = shell.recv(2048)
        output_str = str(output, 'utf-8').replace('\r', '')
        output = re.findall('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} .*',
                            output_str)  # filtering out IPs and host names

        send_message("Active DHCP leases: \n" + "\n".join(str(x) for x in output), chat)

        online_hosts = []
        for host in output:
            ip = re.findall('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', host)[0]
            res = subprocess.call(['ping', '-c', '1', ip], stdout=subprocess.DEVNULL)
            if res == 0:
                online_hosts.append(host)
            elif res == 2:
                pass
            else:
                pass

        send_message("Reachable hosts: \n" + "\n".join(str(x) for x in online_hosts) + "\n\n Done.", chat)
